import greenfoot.*;

/**
 * The class DataBus simulates a connector or set of wires, 
 * that provides transportation for data between CPU and RAM.
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
public class DataBus extends Actor{

    /**
     * This operation is a method. It is used by Greenfoot 
     * to execute the behavior of DataBus object into the game.
     */
    @Override
    public void act(){

    }    

}
