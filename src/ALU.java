import greenfoot.*;

/**
 * The ALU class simulates the Arithmetic Logic Unit that handles 
 * arithmetic and logical operations
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
public class ALU extends Actor{

    /**
     * This operation is a method. It is used by Greenfoot 
     * to execute the behavior of ALU object into the game.
     */
    @Override
    public void act(){

    }

}
