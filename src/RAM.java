import greenfoot.*;

/**
 * The RAM class simulates the part of a computer in which information 
 * is stored while you are using it. RAM is an abbreviation for 'Random 
 * Access Memory'. It is the computer memory available to the user for 
 * creating, loading, or running programs and for the temporary storage 
 * and manipulation of data, in which time of access to each item is 
 * independent of the storage sequence. As a storage medium, RAM is 
 * volatile, so its contents are lost when the power fails or is turned 
 * off.
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
public class RAM extends Actor{

    /**
     * This operation is a method. It is used by Greenfoot 
     * to execute the behavior of Word object into the game.
     */
    @Override
    public void act(){

    }

}
