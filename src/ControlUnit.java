import greenfoot.*;

/**
 * The ControUnit class simulates the part of CPU that extracts instructions 
 * from memory and decodes and executes them
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
public class ControlUnit extends Actor{

    /**
     * This operation is a method. It is used by Greenfoot 
     * to execute the behavior of ControlUnit object into the game.
     */
    @Override
    public void act(){

    }

}
