import greenfoot.*;

/**
 * The CPU class simulates the central processing unit of a computer. 
 * It is the unit which performs most of the processing inside a 
 * computer. To control instructions and data flow to and from other 
 * parts of the computer, the CPU relies heavily on a chipset, which 
 * is a group of microchips located on the motherboard. The CPU has 
 * some registers and two components:
 *
 *  - Control Unit: extracts instructions from memory 
 *                  and decodes and executes them
 *  
 *  - Arithmetic Logic Unit (ALU): handles arithmetic and
 *                                 logical operations
 *
 * To function properly, the CPU relies on the system clock, memory, 
 * secondary storage, data bus and address bus.
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
public class CPU extends Actor{

    /**
     * This is a attribute. It use to store the control unit of the CPU. 
     * It is private so that the use of the class is safer.
     */
    private ControlUnit controlUnit;

    /**
     * This operation is a method. It is used by Greenfoot 
     * to execute the behavior of CPU object into the game.
     */
    @Override
    public void act(){

    }

    /**
     * This method is a getter of the control unit (attribute)
     * 
     * @return the current control unit
     */
    public ControlUnit getControlUnit() {
        return controlUnit;
    }

    /**
     * This method is a setter of the control unit (attribute)
     * 
     * @param controlUnit is the new value of the attribute controlUnit
     */
    public void setControlUnit(ControlUnit controlUnit) {
        this.controlUnit = controlUnit;
    }

    /**
     * This method is a getter of the ALU (attribute)
     * 
     * @return the current ALU
     */
    public ALU getALU() {
        return null;
    }

    /**
     * This method is a setter of the ALU (attribute)
     * 
     * @param alu is the new value of the attribute alu
     */
    public void setALU(ALU alu) {
    	
    }

}
