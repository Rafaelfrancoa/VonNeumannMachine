import greenfoot.*;

/**
 * The class AddressBus simulates a collection of wires connecting 
 * the CPU with RAM that is used to identify particular locations 
 * (addresses) in RAM. 
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
public class AddressBus extends Actor{

    /**
     * This operation is a method. It is used by Greenfoot 
     * to execute the behavior of AddressBus object into the game.
     */
    @Override
    public void act(){    	

    }

}
