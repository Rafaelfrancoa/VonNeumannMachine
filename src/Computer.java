import greenfoot.*;

/**
 * The Computer class is the world of the Greenfoot scenario that simulates von Neumann 
 * architecture. this architecture, also known as the Princeton architecture, is a computer 
 * architecture based on that described in 1945 by the mathematician and physicist John 
 * von Neumann. He described an architecture for an electronic digital computer with parts 
 * consisting of a processing unit containing an arithmetic logic unit (ALU) and processor 
 * registers, a control unit containing an instruction register and program counter (PC), 
 * a memory to store both data and instructions, external mass storage, and input and output 
 * mechanisms (Address Bus, Data Bus and Input/Output ports). 
 * 
 * https://en.wikipedia.org/wiki/Von_Neumann_architecture
 * https://www.sciencedirect.com/topics/computer-science/von-neumann-architecture
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
public class Computer extends World {
    
    /**
     * This is a attribute. It use to store the CPU of the Computer. 
     * It is private so that the use of the class is safer.
     */
    private CPU cpu;

    /**
     * This is a attribute. It use to store the RAM of the Computer. 
     * It is private so that the use of the class is safer.
     */
    private RAM ram;

    /**
     * This is a attribute. It use to store the address bus of the Computer. 
     * It is private so that the use of the class is safer.
     */
    private AddressBus addressBus;

    /**
     * This is a attribute. It use to store the data bus of the Computer. 
     * It is private so that the use of the class is safer.
     */
    private DataBus dataBus;

    /**
     * This operation is a constructor of Greenfoot Word. Greenfoot 
     * needs at least one constructor in the world to have no parameters 
     * since it will be the constructor that will be used to start the 
     * game (or simulation). This constructor must create the actors start
     * the game (or the simulation) and must indicate the that dimensions 
     * of the space where these actors are going to move or place.
     */
    public Computer(){
        super(950, 500, 1);
        prepare();
    }

    /**
     * This operation is a private method because it is only used within 
     * this class. The function of private methods is to break the code 
     * into smaller pieces so that theirs operations can be better understood. 
     * They are also used to avoid repeating the same code in different 
     * methods that have a common part and thereby decrease the probability 
     * of errors. The prepare method is usually used by the constructor to 
     * create the actors that start the game (or the simulation). In this 
     * case, this method creates the parts of a VonNewmann machine: Data Bus, 
     * Address Bus, RAM and CPU (with its ALU and its Control Unit).
     */
    private void prepare(){
        addressBus = new AddressBus();
        addObject(addressBus, 585, 160);

        dataBus = new DataBus();
        addObject(dataBus, 585, 300);

        ram = new RAM();
        addObject(ram, 810, 250);

        cpu = new CPU();
        addObject(cpu, 250, 250);
        
        ControlUnit controlUnit = new ControlUnit();
    	addObject(controlUnit, cpu.getX()+70, cpu.getY()-120);
    	cpu.setControlUnit(controlUnit);
    	
    	ALU alu = new ALU();
    	addObject(alu, cpu.getX()-10, cpu.getY()+140);
    }

    /**
     * This method is a getter of the CPU (attribute)
     * 
     * @return the current CPU
     */
    public CPU getCPU() {
        return cpu;
    }

    /**
     * This method is a getter of the RAM (attribute)
     * 
     * @return the current RAM
     */
    public RAM getRAM() {
        return null;
    }

    /**
     * This method is a getter of the address bus (attribute)
     * 
     * @return the current address bus
     */
    public AddressBus getAddressBus() {
        return addressBus;
    }

    /**
     * This method is a getter of the data bus (attribute)
     * 
     * @return the current data bus
     */
    public DataBus getDataBus() {
        return dataBus;
    }

}
