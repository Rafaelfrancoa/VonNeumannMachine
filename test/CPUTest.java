import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.GreenfootImage;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
 * The class CPUTest verifies the CPU class.
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
@RunWith(GreenfootRunner.class)
public class CPUTest {
    
    /*
     * Common part of the given for all tests 
     */
    Computer computer;
    CPU      cpu;

    /*
     * Common part of the given for all tests 
     */
    @Before
    public void setUp() throws Exception {
        computer = new Computer();
        cpu      = computer.getCPU();
    }

    /*
     * The default image is "CPU.png".
     */
    @Test
    public void testImage() {
        // Given
        GreenfootImage image = cpu.getImage();

        // When
        int width   = image.getWidth();
        int height  = image.getHeight();
        String name = image.getImageFileName();

        // Then
        assertEquals(488, width);
        assertEquals(490, height);
        assertEquals("CPU.png", name);
    }

    /*
     * The parts of CPU are control unit and ALU.
     */
    @Test
    public void testComponents() throws Exception {
        // Given

        // When

        // Then
        assertEquals(1, computer.getObjects(ControlUnit.class).size());
        assertEquals(1, computer.getObjects(ALU.class).size());
    }

    /*
     * The CPU must be in left of computer
     */
    @Test
    public void testCPU_Position() throws Exception {
        // Given

        // When

        // Then
        assertEquals(250, cpu.getX());
        assertEquals(250, cpu.getY());
    }

    
    // Exercise 5: Explain why the result of testControlUnit_Position
    //             is GREEN (the exercise 1 is in ComputerTest file).
    //---------------------------------------------------------------
	
    /*
     * The control unit must be in right up of CPU
     */
    @Test
    public void testControlUnit_Position() throws Exception {
        // Given
        ControlUnit controlUnit = cpu.getControlUnit();

        //When
        int x = controlUnit.getX();
        int y = controlUnit.getY();

        // Then
        assertEquals(cpu.getX()+70,  x);
        assertEquals(cpu.getY()-120, y);
    }
    

    // Exercise 6: Analyze the CPU class and explain how and who 
    //             starts the value of the controlUnit attribute.
    //-----------------------------------------------------------
	

    
    // Exercise 7: Why does testALU_Position fail? 
    //             Modify the CPU class to work (GREEN step).
    //-----------------------------------------------------------
	
    /*
     * The ALU must be in down of CPU
     */
    @Test
    public void testALU_Position() throws Exception {
        // Given
        ALU alu = cpu.getALU();

        //When
        int x = alu.getX();
        int y = alu.getY();

        // Then
        assertEquals(cpu.getX()-10,  x);
        assertEquals(cpu.getY()+140, y);
    }
    
    
    // Exercise 8: Use getters in the rest of test.
    //---------------------------------------------
}
