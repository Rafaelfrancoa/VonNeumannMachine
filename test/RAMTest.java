import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.GreenfootImage;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
 * The class RAMTest verifies the RAM class.
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
@RunWith(GreenfootRunner.class)
public class RAMTest {
    
    /*
     * Common part of the given for all tests 
     */
    Computer computer;
    RAM      ram;

    /*
     * Common part of the given for all tests 
     */
    @Before
    public void setUp() throws Exception {
        computer = new Computer();
        ram      = computer.getObjects(RAM.class).get(0);
    }

    /*
     * The default image is "RAM.png".
     */
    @Test
    public void testImage() {
        // Given
        GreenfootImage image = ram.getImage();

        // When
        int width   = image.getWidth();
        int height  = image.getHeight();
        String name = image.getImageFileName();

        // Then
        assertEquals(271, width);
        assertEquals(490, height);
        assertEquals("RAM.png", name);
    }

}
