import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.GreenfootImage;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
 * The class ControlUnitTest verifies the ControlUnit class.
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
@RunWith(GreenfootRunner.class)
public class ControlUnitTest {
    
    /*
     * Common part of the given for all tests 
     */
    Computer    computer;
    ControlUnit controlUnit;

    /*
     * Common part of the given for all tests 
     */
    @Before
    public void setUp() throws Exception {
        computer    = new Computer();
        controlUnit = computer.getObjects(ControlUnit.class).get(0);
    }

    /*
     * The default image is "ControlUnit.png".
     */
    @Test
    public void testImage() {
        // Given
        GreenfootImage image = controlUnit.getImage();

        // When
        int width   = image.getWidth();
        int height  = image.getHeight();
        String name = image.getImageFileName();

        // Then
        assertEquals(188, width);
        assertEquals(167, height);
        assertEquals("ControlUnit.png", name);
    }

}
