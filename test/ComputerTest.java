import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.GreenfootImage;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
* The class ComputerTest verifies the Calculator class.
* 
* @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
* @version 1.0
*/
@RunWith(GreenfootRunner.class)
public class ComputerTest {

    /*
     * Common part of the given for all tests 
     */
    Computer computer;

    /*
     * Common part of the given for all tests 
     */
    @Before
    public void setUp() throws Exception {
        computer = new Computer();
    }

    /*
     * The computer background image must fit on the screen.
     */
    @Test
    public void testComputerSize() throws Exception {
        // Given

        // When

        // Then
        assertEquals(950, computer.getWidth());
        assertEquals(500, computer.getHeight());
    }

    /*
     * The default image is "Computer.jpg".
     */
    @Test
    public void testImage() throws Exception {
        // Given
        GreenfootImage image = computer.getBackground();

        // When
        String name = image.getImageFileName();

        // Then
        assertEquals("Computer.jpg", name);
    }

    /*
     * The parts of computer are CPU (with its control unit and its ALU), 
     * address bus, data bus and RAM
     */
    @Test
    public void testComponents() throws Exception {
        // Given

        // When

        // Then
        assertEquals(1, computer.getObjects(CPU.class).size());
        assertEquals(1, computer.getObjects(ControlUnit.class).size());
        assertEquals(1, computer.getObjects(ALU.class).size());
        assertEquals(1, computer.getObjects(AddressBus.class).size());
        assertEquals(1, computer.getObjects(DataBus.class).size());
        assertEquals(1, computer.getObjects(RAM.class).size());
    }

    
    // Exercise 1: Compare testCPU_Position and testCPU_PositionBis 
    //             and explain why the result of both tests are GREEN
    //---------------------------------------------------------------
	
    /*
     * The CPU must be in left of computer
     */
    @Test
    public void testCPU_Position() throws Exception {
        // Given
        CPU cpu = computer.getObjects(CPU.class).get(0);

        //When
        int x = cpu.getX();
        int y = cpu.getY();

        // Then
        assertEquals(250, x);
        assertEquals(250, y);
    }

    /*
     * The CPU must be in left of computer
     */
    @Test
    public void testCPU_PositionBis() throws Exception {
        // Given
        CPU cpu = computer.getCPU();

        //When
        int x = cpu.getX();
        int y = cpu.getY();

        //Then
        assertEquals(250, x);
        assertEquals(250, y);
    }

    
    // Exercise 2: Use getter of AddressBuss in testAddressBus_Position 
    //-----------------------------------------------------------------
	
    /*
     * The address bus must be in center up of computer
     */
    @Test
    public void testAddressBus_Position() throws Exception {
        // Given
        AddressBus addressBus = computer.getObjects(AddressBus.class).get(0);

        // When
        int x = addressBus.getX();
        int y = addressBus.getY();

        // Then
        assertEquals(585, x);
        assertEquals(160, y);
    }

    
    // Exercise 3: Use getter of DataBus in testDataBus_Position 
    //-----------------------------------------------------------
	
    /*
     * The data bus must be in center down of computer
     */
    @Test
    public void testDataBus_Position() throws Exception {
        // Given
        DataBus dataBus = computer.getObjects(DataBus.class).get(0);

        // When
        int x = dataBus.getX();
        int y = dataBus.getY();

        // Then
        assertEquals(585, x);
        assertEquals(300, y);
    }
    
    
    // Exercise 4: Use getter of RAM in testRAM_Position 
    //----------------------------------------------------
	
   /*
     * The RAM must be right of computer
     */
    @Test
    public void testRAM_Position() throws Exception {
        // Given
        RAM ram = computer.getObjects(RAM.class).get(0);

        // When
        int x = ram.getX();
        int y = ram.getY();

        // Then
        assertEquals(810, x);
        assertEquals(250, y);
    }
}
