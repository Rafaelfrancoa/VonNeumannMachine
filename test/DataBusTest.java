import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.GreenfootImage;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
 * The class DataBusTest verifies the DataBus class.
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
@RunWith(GreenfootRunner.class)
public class DataBusTest {
    
    /*
     * Common part of the given for all tests 
     */
    Computer computer;
    DataBus  dataBus;

    /*
     * Common part of the given for all tests 
     */
    @Before
    public void setUp() throws Exception {
        computer = new Computer();
        dataBus  = computer.getObjects(DataBus.class).get(0);
    }

    /*
     * The default image is "DataBus.png".
     */
    @Test
    public void testImage() {
        // Given
        GreenfootImage image = dataBus.getImage();

        // When
        int width   = image.getWidth();
        int height  = image.getHeight();
        String name = image.getImageFileName();

        // Then
        assertEquals(174, width);
        assertEquals( 72, height);
        assertEquals("DataBus.png", name);
    }

}
