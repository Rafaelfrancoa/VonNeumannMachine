import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.GreenfootImage;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
 * The class ALUTest vreifies the ALU class.
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
@RunWith(GreenfootRunner.class)
public class ALUTest {
    
    /*
     * Common part of the given for all tests 
     */
    Computer computer;
    ALU      alu;

    /*
     * Common part of the given for all tests 
     */
    @Before
    public void setUp() throws Exception {
        computer = new Computer();
        alu      = computer.getObjects(ALU.class).get(0);
    }

    /*
     * The default image is "ALU.png".
     */
    @Test
    public void testImage() {
        // Given
        GreenfootImage image = alu.getImage();

        // When
        int width   = image.getWidth();
        int height  = image.getHeight();
        String name = image.getImageFileName();

        // Then
        assertEquals(423, width);
        assertEquals(171, height);
        assertEquals("ALU.png", name);
    }

}
