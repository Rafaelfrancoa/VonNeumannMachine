import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.GreenfootImage;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
 * The class AddressBusTest verifies the AddressBus class
 * 
 * @author Francisco Guerra (Francisco.Guerra@ulpgc.es) 
 * @version 1.0
 */
@RunWith(GreenfootRunner.class)
public class AddressBusTest {
    
    /*
     * Common part of the given for all tests 
     */
    Computer   computer;
    AddressBus addressBus;

    /*
     * Common part of the given for all tests 
     */
    @Before
    public void setUp() throws Exception {
        computer   = new Computer();
        addressBus = computer.getObjects(AddressBus.class).get(0);
    }

    /*
     * The default image is "AddressBus.png".
     */
    @Test
    public void testImage() {
        // Given
        GreenfootImage image = addressBus.getImage();

        // When
        int width   = image.getWidth();
        int height  = image.getHeight();
        String name = image.getImageFileName();

        // Then
        assertEquals(180, width);
        assertEquals( 72, height);
        assertEquals("AddressBus.png", name);
    }

}
